package com.jeesuite.bestpl.exception;

public class ErrorResponseEntity{
	
	public ErrorResponseEntity(){}


	public ErrorResponseEntity(int code, String msg) {
		this.errorCode = code;
		this.errorMsg = msg;
	}

 

	// 状态
	private int errorCode = 501;

	// 返回信息
	private String errorMsg;

	public int getErrorCode() {
		return errorCode;
	}


	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}


	public String getErrorMsg() {
		return errorMsg;
	}


	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	
	
}
