/**
 * 
 */
package com.jeesuite.bestpl.api;

import java.util.List;

import com.jeesuite.bestpl.dto.Comment;
import com.jeesuite.bestpl.dto.IdNamePair;
import com.jeesuite.bestpl.dto.Page;
import com.jeesuite.bestpl.dto.PageQueryParam;
import com.jeesuite.bestpl.dto.Post;

/**
 * @description <br>
 * @author <a href="mailto:vakinge@gmail.com">vakin</a>
 * @date 2016年10月25日
 */
public interface IPostService {
	
	List<IdNamePair> findAllPostCategory();
	
	void addPosts(Post post);
	
	Post findPostById(int id);
	
	List<Post> findTopPost(int categoryId,String sortType,int top);
	
	Page<Post> pageQueryPost(final PageQueryParam param);
	
	void addPostComment(Comment comment);
	
	Page<Comment> pageQueryPostComment(int postId,int pageNo,int pageSize);
	
	List<String> findHotTags(int limit);
}
